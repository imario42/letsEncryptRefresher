package at.datenwort.letsEncryptRefresher;

import org.apache.catalina.connector.Connector;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Order;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Status;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.challenge.Http01Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.shredzone.acme4j.util.CSRBuilder;
import org.shredzone.acme4j.util.KeyPairUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AcmeClient
{
    public enum AcmeResult
    {
        FAILED,

        VALID,

        REGENERATED
    }

    public interface AcmeTokenStore
    {
        void setChallengeToken(String name, String data);
    }

    private final static Logger log = Logger.getLogger(AcmeClient.class.getName());

    private final boolean staging;

    private final Collection<String> domains;

    private final File domainCsrFile;
    private final File domainChainFile;
    private final File domainKeyFile;
    private final File userKeyFile;

    private final File keystoreFile;
    private final String keystorePassword;
    private final String keystoreAlias;

    private final AcmeTokenStore acmeTokenStore;

    private final int KEY_SIZE = 2048;

    private final Connector connector;
    public AcmeClient(boolean staging,
                      Collection<String> domains,
                      File acmeStore,
                      AcmeTokenStore acmeTokenStore,
                      File keystoreFile,
                      String keystorePassword,
                      String keystoreAlias,
                      Connector connector)
    {
        this.staging = staging;

        this.domains = Collections.unmodifiableCollection(domains);

        this.acmeTokenStore = acmeTokenStore;

        this.domainCsrFile = new File(acmeStore, "domain_csr_file.bin");
        this.domainChainFile = new File(acmeStore, "domain_chain_file.bin");
        this.domainKeyFile = new File(acmeStore, "domain_key_file.bin");
        this.userKeyFile = new File(acmeStore, "user_key_file.bin");

        this.keystoreFile = keystoreFile;
        this.keystorePassword = keystorePassword;
        this.keystoreAlias = keystoreAlias;

        this.connector = connector;
    }

    public Collection<String> getDomains()
    {
        return domains;
    }

    public Connector getConnector()
    {
        return connector;
    }

    /**
     * Generates a certificate for the given domains. Also takes care for the registration
     * process.
     */
    public AcmeResult fetchCertificate() throws IOException, AcmeException
    {
        try
        {
            if (domains.size() < 1)
            {
                return AcmeResult.FAILED;
            }

            if (!requireRegenerating())
            {
                return AcmeResult.VALID;
            }

            // Load or create a key pair for the domains. This should not be the userKeyPair!
            KeyPair domainKeyPair = loadOrCreateDomainKeyPair();

            // Load the user key file. If there is no key file, create a new one.
            KeyPair userKeyPair = loadOrCreateUserKeyPair();

            // Create a session for Let's Encrypt.
            // Use "acme://letsencrypt.org" for production server
            Session session;
            if (staging)
            {
                session = new Session("acme://letsencrypt.org/staging");
            }
            else
            {
                session = new Session("acme://letsencrypt.org/");
            }

            // Get the Account.
            // If there is no account yet, create a new one.
            Account acct = findOrRegisterAccount(session, userKeyPair);


            // Order the certificate
            Order order = acct.newOrder().domains(domains).create();

            // Perform all required authorizations
            AcmeResult overallAcmeResult = null;
            for (Authorization auth : order.getAuthorizations())
            {
                AcmeResult acmeResult = authorize(auth);

                if (overallAcmeResult != AcmeResult.REGENERATED
                    && overallAcmeResult != AcmeResult.FAILED)
                {
                    overallAcmeResult = acmeResult;
                }
            }

            // Generate a CSR for all of the domains, and sign it with the domain key pair.
            CSRBuilder csrb = new CSRBuilder();
            csrb.addDomains(domains);
            csrb.sign(domainKeyPair);

            // Write the CSR to a file, for later use.
            try (Writer out = new FileWriter(domainCsrFile))
            {
                csrb.write(out);
            }

            // Order the certificate
            order.execute(csrb.getEncoded());

            // Wait for the order to complete
            try
            {
                int attempts = 10;
                while (order.getStatus() != Status.VALID && attempts-- > 0)
                {
                    // Did the order fail?
                    if (order.getStatus() == Status.INVALID)
                    {
                        throw new AcmeException("Order failed... Giving up.");
                    }

                    // Wait for a few seconds
                    Thread.sleep(3000L);

                    // Then update the status
                    order.update();
                }
            }
            catch (InterruptedException ex)
            {
                log.log(Level.FINE, "interrupted", ex);
                Thread.currentThread().interrupt();
            }

            // Get the certificate
            org.shredzone.acme4j.Certificate certificate = order.getCertificate();

            log.info("Success! The certificate for domains " + domains + " has been generated!");
            log.info("Certificate URL: " + certificate.getLocation());

            // Write a combined file containing the certificate and chain.
            try (FileWriter fw = new FileWriter(domainChainFile))
            {
                certificate.writeCertificate(fw);
            }

            if (overallAcmeResult == AcmeResult.REGENERATED)
            {
                try
                {
                    exportToKeystore();
                }
                catch (Exception e)
                {
                    throw new AcmeException("problems converting to keystore", e);
                }
            }

            return overallAcmeResult;
        }
        finally
        {
            acmeTokenStore.setChallengeToken(null, null);
        }
    }

    public void exportToKeystore() throws Exception
    {
        try (FileReader keyReader = new FileReader(domainKeyFile);
             PEMParser keyParser = new PEMParser(keyReader);
             FileInputStream cerFileStream = new FileInputStream(domainChainFile))
        {
            PEMKeyPair pemKeyPair = ((PEMKeyPair) keyParser.readObject());
            JcaPEMKeyConverter jcaPEMKeyConverter = new JcaPEMKeyConverter();
            KeyPair keyPair = jcaPEMKeyConverter.getKeyPair(pemKeyPair);

            PrivateKey key = keyPair.getPrivate();

            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            Collection<? extends Certificate> generatedCertificates = fact.generateCertificates(cerFileStream);
            Certificate[] certificates = generatedCertificates.toArray(new Certificate[0]);

            try (FileOutputStream fos = new FileOutputStream(keystoreFile, false))
            {
                KeyStore ks = KeyStore.getInstance("PKCS12");
                ks.load(null);
                ks.setKeyEntry(keystoreAlias, key, keystorePassword.toCharArray(), certificates);
                ks.store(fos, keystorePassword.toCharArray());
            }
        }
    }

    public boolean requireRegenerating() throws AcmeException
    {
        if (!domainChainFile.exists())
        {
            return true;
        }

        try
        {
            try (FileInputStream cerFileStream = new FileInputStream(domainChainFile))
            {
                CertificateFactory fact = CertificateFactory.getInstance("X.509");
                Collection<? extends Certificate> generatedCertificates = fact.generateCertificates(cerFileStream);
                Certificate[] certificates = generatedCertificates.toArray(new Certificate[0]);

                Boolean requireRegenerating = null;

                for (Certificate certificate : certificates)
                {
                    if (!(certificate instanceof X509Certificate))
                    {
                        continue;
                    }

                    X509Certificate x509Certificate = (X509Certificate) certificate;
                    String hostname = x509Certificate.getSubjectDN() != null ? x509Certificate.getSubjectDN().getName() : null;
                    if (hostname == null || !hostname.startsWith("CN="))
                    {
                        continue;
                    }
                    hostname = hostname.substring(3);
                    if (!domains.contains(hostname))
                    {
                        continue;
                    }

                    LocalDate notAfter = LocalDate.ofInstant(x509Certificate.getNotAfter().toInstant(), ZoneId.systemDefault()).minusWeeks(3);
                    if (!notAfter.isAfter(LocalDate.now()))
                    {
                        requireRegenerating = true;
                    }
                    else if (requireRegenerating == null)
                    {
                        requireRegenerating = false;
                    }
                }

                return requireRegenerating == null || requireRegenerating;
            }
        }
        catch (FileNotFoundException e)
        {
            return true;
        }
        catch (IOException e)
        {
            throw new AcmeException("problems reading domain chain file", e);
        }
        catch (CertificateException e)
        {
            throw new AcmeException("problems reading domain chain certificates", e);
        }
    }

    private KeyPair loadOrCreateUserKeyPair() throws IOException
    {
        if (userKeyFile.exists())
        {
            // If there is a key file, read it
            try (FileReader fr = new FileReader(userKeyFile))
            {
                return KeyPairUtils.readKeyPair(fr);
            }

        }
        else
        {
            // If there is none, create a new key pair and save it
            KeyPair userKeyPair = KeyPairUtils.createKeyPair(KEY_SIZE);
            try (FileWriter fw = new FileWriter(userKeyFile))
            {
                KeyPairUtils.writeKeyPair(userKeyPair, fw);
            }
            return userKeyPair;
        }
    }

    private Account findOrRegisterAccount(Session session, KeyPair accountKey) throws AcmeException
    {
        // Ask the user to accept the TOS, if server provides us with a link.
        URI tos = session.getMetadata().getTermsOfService();
        // silently accept TOS

        Account account = new AccountBuilder()
            .agreeToTermsOfService()
            .useKeyPair(accountKey)
            .create(session);
        log.info("Registered a new user, URL: " + account.getLocation());

        return account;
    }

    private KeyPair loadOrCreateDomainKeyPair() throws IOException
    {
        if (domainKeyFile.exists())
        {
            try (FileReader fr = new FileReader(domainKeyFile))
            {
                return KeyPairUtils.readKeyPair(fr);
            }
        }
        else
        {
            KeyPair domainKeyPair = KeyPairUtils.createKeyPair(KEY_SIZE);
            try (FileWriter fw = new FileWriter(domainKeyFile))
            {
                KeyPairUtils.writeKeyPair(domainKeyPair, fw);
            }
            return domainKeyPair;
        }
    }

    /**
     * Authorize a domain. It will be associated with your account, so you will be able to
     * retrieve a signed certificate for the domain later.
     *
     * @param auth {@link Authorization} to perform
     */
    private AcmeResult authorize(Authorization auth) throws AcmeException
    {
        log.info("Authorization for domain " + auth.getDomain());

        // The authorization is already valid. No need to process a challenge.
        if (auth.getStatus() == Status.VALID)
        {
            return AcmeResult.VALID;
        }

        // Find the desired challenge and prepare it.
        Challenge challenge = httpChallenge(auth);
        if (challenge == null)
        {
            throw new AcmeException("No challenge found");
        }

        // If the challenge is already verified, there's no need to execute it again.
        if (challenge.getStatus() == Status.VALID)
        {
            return AcmeResult.VALID;
        }

        // Now trigger the challenge.
        challenge.trigger();

        // Poll for the challenge to complete.
        try
        {
            int attempts = 10;
            while (challenge.getStatus() != Status.VALID && attempts-- > 0)
            {
                // Did the authorization fail?
                if (challenge.getStatus() == Status.INVALID)
                {
                    throw new AcmeException("Challenge failed... Giving up.");
                }

                // Wait for a few seconds
                Thread.sleep(3000L);

                // Then update the status
                challenge.update();
            }
        }
        catch (InterruptedException ex)
        {
            log.log(Level.FINE, "interrupted", ex);
            Thread.currentThread().interrupt();
        }

        // All reattempts are used up and there is still no valid authorization?
        if (challenge.getStatus() != Status.VALID)
        {
            throw new AcmeException("Failed to pass the challenge for domain "
                                    + auth.getDomain() + ", ... Giving up.");
        }

        return AcmeResult.REGENERATED;
    }

    public Challenge httpChallenge(Authorization auth) throws AcmeException
    {
        // Find a single http-01 challenge
        Http01Challenge challenge = auth.findChallenge(Http01Challenge.TYPE);
        if (challenge == null)
        {
            throw new AcmeException("Found no " + Http01Challenge.TYPE + " challenge, don't know what to do...");
        }

        acmeTokenStore.setChallengeToken(challenge.getToken(), challenge.getAuthorization());

        return challenge;
    }
}
