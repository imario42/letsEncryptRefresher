package at.datenwort.letsEncryptRefresher;

import org.apache.catalina.Service;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardHost;
import org.apache.catalina.core.StandardServer;
import org.apache.tomcat.util.net.SSLHostConfig;
import org.apache.tomcat.util.net.SSLHostConfigCertificate;
import org.shredzone.acme4j.exception.AcmeException;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AcmeServlet extends HttpServlet
{
    private final static Logger log = Logger.getLogger(AcmeServlet.class.getName());

    private boolean acmeStaging;

    private String name;
    private String authorization;

    private List<AcmeClient> acmeClients = new ArrayList<>();

    private Timer timer = new Timer();

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        this.acmeStaging = !"true".equals(config.getInitParameter("acmeProduction"));

        try
        {
            Set<String> defaultHostNamesToGenerate = new HashSet<>();
            boolean defaultSSLConnectorSeen = false;

            MBeanServer mbeanServer = ManagementFactory.getPlatformMBeanServer();
            Set<ObjectName> mbeanHostNames = mbeanServer.queryNames(new ObjectName("*:type=Host,*"), null);
            for (ObjectName mbeanHostName : mbeanHostNames)
            {
                Object instance = mbeanServer.getAttribute(mbeanHostName, "managedResource");
                if (!(instance instanceof StandardHost))
                {
                    continue;
                }

                StandardHost standardHost = (StandardHost) instance;
                checkAndAddHost(defaultHostNamesToGenerate, standardHost.getName());
                for (String alias : standardHost.getAliases())
                {
                    checkAndAddHost(defaultHostNamesToGenerate, alias);
                }
            }

            Set<ObjectName> mbeanServerNames = mbeanServer.queryNames(new ObjectName("*:type=Server"), null);

            for (ObjectName mbeanServerName : mbeanServerNames)
            {
                Object instance = mbeanServer.getAttribute(mbeanServerName, "managedResource");
                if (!(instance instanceof StandardServer))
                {
                    continue;
                }

                StandardServer standardServer = (StandardServer) instance;

                for (Service service : standardServer.findServices())
                {

                    for (Connector connector : service.findConnectors())
                    {
                        for (SSLHostConfig sslHostConfig : connector.findSslHostConfigs())
                        {
                            Set<String> hostNamesToGenerate;
                            if (!"_default_".equalsIgnoreCase(sslHostConfig.getHostName()))
                            {
                                hostNamesToGenerate = new HashSet<>();
                                checkAndAddHost(hostNamesToGenerate, sslHostConfig.getHostName());

                                // this host gets its own certificate, remove it from the list of default host names
                                defaultHostNamesToGenerate.remove(sslHostConfig.getHostName());
                            }
                            else
                            {
                                if (defaultSSLConnectorSeen)
                                {
                                    log.log(Level.INFO, "ssl connector for default hosts already seen. skipping {0}.", sslHostConfig.getObjectName());
                                    continue;
                                }

                                defaultSSLConnectorSeen = true;
                                hostNamesToGenerate = defaultHostNamesToGenerate;
                            }

                            if (hostNamesToGenerate.size() < 1)
                            {
                                // no valid host names seen
                                continue;
                            }

                            for (SSLHostConfigCertificate certificate : sslHostConfig.getCertificates(false))
                            {
                                if (certificate.getType() != SSLHostConfigCertificate.Type.RSA
                                    || certificate.getCertificateKeystoreFile() == null
                                    || certificate.getCertificateKeystoreFile().trim().length() < 1)
                                {
                                    continue;
                                }

                                File keystoreFile = new File(certificate.getCertificateKeystoreFile());
                                if ((keystoreFile.exists() && !keystoreFile.canWrite())
                                    || !keystoreFile.getParentFile().canWrite())
                                {
                                    log.log(Level.INFO, "certificate {0} file not writable.", certificate.getCertificateKeystoreFile());
                                    continue;
                                }

                                File acmeStore = new File(keystoreFile.getParentFile(), "acmeStore");
                                if (!acmeStore.exists())
                                {
                                    acmeStore.mkdir();
                                }

                                AcmeClient acmeClient = new AcmeClient(
                                    acmeStaging,
                                    hostNamesToGenerate,
                                    acmeStore,
                                    AcmeServlet.this::setChallengeToken,
                                    keystoreFile,
                                    certificate.getCertificateKeystorePassword(),
                                    certificate.getCertificateKeyAlias() != null ? certificate.getCertificateKeyAlias() : "tomcat",
                                    connector);
                                acmeClients.add(acmeClient);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }

        // after processing all ssl connectors no default hosts might be left, remove those clients again.
        acmeClients.removeIf(acmeClient -> acmeClient.getDomains().size() < 1);

        timer.schedule(new TimerTask()
                       {
                           @Override
                           public void run()
                           {
                               acmeClients.forEach(acmeClient -> {
                                   try
                                   {
                                       log.log(Level.INFO, "Checking certificate for {0}", acmeClient.getDomains());

                                       AcmeClient.AcmeResult result = acmeClient.fetchCertificate();
                                       if (result == AcmeClient.AcmeResult.REGENERATED)
                                       {
                                           log.log(Level.INFO, "Certificate for {0} regenerated. Restarting connector.", acmeClient.getDomains());

                                           acmeClient.getConnector().stop();
                                           // TODO: probably we have something to do to force reloading the keystore?
                                           acmeClient.getConnector().start();
                                       }
                                       else if (result == AcmeClient.AcmeResult.VALID)
                                       {
                                           log.log(Level.INFO, "Certificate for {0} still valid.", acmeClient.getDomains());
                                       }
                                       else
                                       {
                                           log.log(Level.INFO, "Checking Certificate for {0} failed for unknown reasons.", acmeClient.getDomains());
                                       }
                                   }
                                   catch (Exception e)
                                   {
                                       log.log(Level.SEVERE, "Problems updating certificate for {0}", acmeClient);
                                       log.log(Level.SEVERE, "Exception when updating certificate", e);
                                   }
                               });
                           }
                       },
            2_000,
            24 * 60 * 60 * 1000);
        super.init(config);
    }

    @Override
    public void destroy()
    {
        timer.cancel();

        super.destroy();
    }

    protected void checkAndAddHost(Set<String> hostnames, String name)
    {
        if (name != null && name.contains("."))
        {
            hostnames.add(name);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        System.err.println("GET: " + req.getRequestURI());

        String pathInfo = req.getPathInfo();
        if (name != null
            && pathInfo != null
            && pathInfo.length() > 0
            && name.equals(pathInfo.substring(1)))
        {
            resp.getWriter().write(authorization);
            resp.setStatus(HttpServletResponse.SC_OK);
            return;
        }

        super.doGet(req, resp);
    }

    private void setChallengeToken(String name, String authorization)
    {
        this.name = name;
        this.authorization = authorization;
    }
}
